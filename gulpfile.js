let gulp = require('gulp');
let sass = require('gulp-sass');

gulp.task('sass', function() {
    gulp.src('./app/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./app/css/'));
});

gulp.task('watch', function() {
    gulp.watch('./app/scss/**/*.scss', ['sass']);
});
