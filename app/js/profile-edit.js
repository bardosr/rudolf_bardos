'use strict';

/**
 * @description Handling the profile editing in desktop and mobile version
 */

export function setupProfileEdit() {
    let id;
    let data;

    // clicking on the pen icon
    $('.profile-info__list-item-edit').on('click', (e) => {

        id = e.currentTarget.id;

        let target = $(e.currentTarget);

        // mobile editing
        if (target.hasClass('is-visible-mobile')) {
            $('.profile-info').removeClass('profile-tabs--active');
            $('.profile-edit').addClass('profile-tabs--active');

            // capturing the profile info from the HTML
            let profileInfo = {
                firstname: $('#rollover-name').data('rollover').split(' ')[0] || '',
                lastname: $('#rollover-name').data('rollover').split(' ')[1] || '',
                website: $('#rollover-website').data('rollover') || '',
                phone: $('#rollover-phone').data('rollover') || '',
                address: $('#rollover-address').data('rollover') || ''
            };

            for (let key in profileInfo) {
                if (profileInfo.hasOwnProperty(key)) {
                    $(`#${key}-edit`).val(profileInfo[key]);
                }
            }

            return;
        }

        // rollover buttons
        let buttons = `
                <button class="btn btn--full btn--save">Save</button>
                <button class="btn btn--outline">Cancel</button>
            `;

        // assembling the rollover template
        let html = $(`#${id}-template`).html() + buttons;

        data = $(e.currentTarget).data('rollover');

        $('.profile-rollover').html(html);

        // show the rollover
        setTimeout(() => {
            $('.profile-rollover').css({
                left: (e.pageX + 10) + 'px',
                top: (e.pageY - 20) + 'px',
                display:'block',
                opacity: 0
            }).animate({
                opacity: 1
            }, 250);

            $('.input-block__input--edit').val(data);
        }, 100);
    });

    // clicking on the empty area hides the rollover
    $('.profile-tabs').on('click', (e) => {
        if ($(e.target).hasClass('ion-edit')) return;

        hideRollover();
    });

    // save the profile info or discard changes desktop version
    $('.profile-rollover').on('click', (e) => {
        let target = $(e.target);
        let newVal;

        if (target.hasClass('btn--save') || target.hasClass('btn--outline')) {
            if (target.hasClass('btn--save')) {
                newVal = $('.input-block__input--edit').val();

                if (newVal && newVal !== '') {
                    $(`#${id}`)
                        .data('rollover', newVal)
                        .prev()
                        .html(newVal);
                }
            }

            hideRollover();
        }
    });


    // save the profile info or discard changes mobile version
    $('.profile-edit').on('click', (e) => {
        let target = e.target;

        if (target.id === 'cancel-button' || target.id === 'save-button') {
            if (target.id === 'save-button') {
                let profileInfo = {
                    name: ($('#firstname-edit').val() || '') + ' ' + ($('#lastname-edit').val() || ''),
                    website: $('#website-edit').val() || '',
                    phone: $('#phone-edit').val() || '',
                    address: $('#address-edit').val() || ''
                };

                for (let key in profileInfo) {
                    if (profileInfo.hasOwnProperty(key)) {
                        $(`#rollover-${key}`).data('rollover', profileInfo[key]);
                        $(`#rollover-${key}`).prev().html(profileInfo[key]);
                    }
                }
            }

            $('.profile-edit').removeClass('profile-tabs--active');
            $('.profile-info').addClass('profile-tabs--active');
        }
    });
}

// helper function to hide the rollover
function hideRollover() {
    $('.profile-rollover').animate({
        opacity: 0
    },250, () => {
        $('.profile-rollover').css('display', 'none').empty();
    });
}