'use strict';

/**
 * @description Swipe left and right event listeners for the navigation menu
 *              on small screen sizes
 */

export function registerMobileEvents() {
    $('.profile-header__navigation-container').swipeleft(function(){
        let navWidth = $(".navigation").width();
        let windowWidth = $(window).innerWidth();

        if (navWidth > windowWidth) {
            $('.navigation').animate({ left: -(navWidth - windowWidth) }, 500);
        }
    });

    $('.profile-header__navigation-container').swiperight(function(){
        let nav = $(".navigation");
        let navWidth = nav.width();
        let windowWidth = $(window).innerWidth();

        if (navWidth > windowWidth && nav.offset().left < 0) {
            $('.navigation').animate({ left: 0 }, 500);
        }
    });
}
