'use strict';

import { registerMobileEvents } from './mobile-events';
import { setupNavigation } from './navigation';
import { setupResize } from './resize';
import { setupProfileEdit } from './profile-edit';

$(() => {
    // bootstrapping the applications
    registerMobileEvents();
    setupNavigation();
    setupResize();
    setupProfileEdit();
});