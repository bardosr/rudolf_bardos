'use strict';

/**
 * @description Handling resizing if the profile edit screen is active on mobile view
 */

export function setupResize() {
    let breakSmall = 768;
    let editEl = $('.profile-edit');

    $(window).on('resize', () => {
        if (window.innerWidth > breakSmall) {
            if (editEl.hasClass('profile-tabs--active')) {
                editEl.removeClass('profile-tabs--active');
                $('.profile-info').addClass('profile-tabs--active');
            }
        }
    });
}
