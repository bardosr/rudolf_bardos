'use strict';

/**
 * @description Handling the navigation tab change events
 */

export function setupNavigation() {
    $('.navigation').on('click', (e) => {
        let id = e.target.id;
        let target = $(e.target);

        if (!target.hasClass('navigation__element')) {
            return;
        }

        $('.navigation__element--active').removeClass('navigation__element--active');
        target.addClass('navigation__element--active');

        $('.profile-tabs--active').removeClass('profile-tabs--active');
        $(`.${id}`).addClass('profile-tabs--active');
    });
}
