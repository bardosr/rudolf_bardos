/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * @description Swipe left and right event listeners for the navigation menu
 *              on small screen sizes
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.registerMobileEvents = registerMobileEvents;
function registerMobileEvents() {
    $('.profile-header__navigation-container').swipeleft(function () {
        var navWidth = $(".navigation").width();
        var windowWidth = $(window).innerWidth();

        if (navWidth > windowWidth) {
            $('.navigation').animate({ left: -(navWidth - windowWidth) }, 500);
        }
    });

    $('.profile-header__navigation-container').swiperight(function () {
        var nav = $(".navigation");
        var navWidth = nav.width();
        var windowWidth = $(window).innerWidth();

        if (navWidth > windowWidth && nav.offset().left < 0) {
            $('.navigation').animate({ left: 0 }, 500);
        }
    });
}

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * @description Handling the navigation tab change events
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setupNavigation = setupNavigation;
function setupNavigation() {
    $('.navigation').on('click', function (e) {
        var id = e.target.id;
        var target = $(e.target);

        if (!target.hasClass('navigation__element')) {
            return;
        }

        $('.navigation__element--active').removeClass('navigation__element--active');
        target.addClass('navigation__element--active');

        $('.profile-tabs--active').removeClass('profile-tabs--active');
        $('.' + id).addClass('profile-tabs--active');
    });
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * @description Handling the profile editing in desktop and mobile version
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setupProfileEdit = setupProfileEdit;
function setupProfileEdit() {
    var id = void 0;
    var data = void 0;

    // clicking on the pen icon
    $('.profile-info__list-item-edit').on('click', function (e) {

        id = e.currentTarget.id;

        var target = $(e.currentTarget);

        // mobile editing
        if (target.hasClass('is-visible-mobile')) {
            $('.profile-info').removeClass('profile-tabs--active');
            $('.profile-edit').addClass('profile-tabs--active');

            // capturing the profile info from the HTML
            var profileInfo = {
                firstname: $('#rollover-name').data('rollover').split(' ')[0] || '',
                lastname: $('#rollover-name').data('rollover').split(' ')[1] || '',
                website: $('#rollover-website').data('rollover') || '',
                phone: $('#rollover-phone').data('rollover') || '',
                address: $('#rollover-address').data('rollover') || ''
            };

            for (var key in profileInfo) {
                if (profileInfo.hasOwnProperty(key)) {
                    $('#' + key + '-edit').val(profileInfo[key]);
                }
            }

            return;
        }

        // rollover buttons
        var buttons = '\n                <button class="btn btn--full btn--save">Save</button>\n                <button class="btn btn--outline">Cancel</button>\n            ';

        // assembling the rollover template
        var html = $('#' + id + '-template').html() + buttons;

        data = $(e.currentTarget).data('rollover');

        $('.profile-rollover').html(html);

        // show the rollover
        setTimeout(function () {
            $('.profile-rollover').css({
                left: e.pageX + 10 + 'px',
                top: e.pageY - 20 + 'px',
                display: 'block',
                opacity: 0
            }).animate({
                opacity: 1
            }, 250);

            $('.input-block__input--edit').val(data);
        }, 100);
    });

    // clicking on the empty area hides the rollover
    $('.profile-tabs').on('click', function (e) {
        if ($(e.target).hasClass('ion-edit')) return;

        hideRollover();
    });

    // save the profile info or discard changes desktop version
    $('.profile-rollover').on('click', function (e) {
        var target = $(e.target);
        var newVal = void 0;

        if (target.hasClass('btn--save') || target.hasClass('btn--outline')) {
            if (target.hasClass('btn--save')) {
                newVal = $('.input-block__input--edit').val();

                if (newVal && newVal !== '') {
                    $('#' + id).data('rollover', newVal).prev().html(newVal);
                }
            }

            hideRollover();
        }
    });

    // save the profile info or discard changes mobile version
    $('.profile-edit').on('click', function (e) {
        var target = e.target;

        if (target.id === 'cancel-button' || target.id === 'save-button') {
            if (target.id === 'save-button') {
                var profileInfo = {
                    name: ($('#firstname-edit').val() || '') + ' ' + ($('#lastname-edit').val() || ''),
                    website: $('#website-edit').val() || '',
                    phone: $('#phone-edit').val() || '',
                    address: $('#address-edit').val() || ''
                };

                for (var key in profileInfo) {
                    if (profileInfo.hasOwnProperty(key)) {
                        $('#rollover-' + key).data('rollover', profileInfo[key]);
                        $('#rollover-' + key).prev().html(profileInfo[key]);
                    }
                }
            }

            $('.profile-edit').removeClass('profile-tabs--active');
            $('.profile-info').addClass('profile-tabs--active');
        }
    });
}

// helper function to hide the rollover
function hideRollover() {
    $('.profile-rollover').animate({
        opacity: 0
    }, 250, function () {
        $('.profile-rollover').css('display', 'none').empty();
    });
}

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * @description Handling resizing if the profile edit screen is active on mobile view
 */

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setupResize = setupResize;
function setupResize() {
    var breakSmall = 768;
    var editEl = $('.profile-edit');

    $(window).on('resize', function () {
        if (window.innerWidth > breakSmall) {
            if (editEl.hasClass('profile-tabs--active')) {
                editEl.removeClass('profile-tabs--active');
                $('.profile-info').addClass('profile-tabs--active');
            }
        }
    });
}

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _mobileEvents = __webpack_require__(0);

var _navigation = __webpack_require__(1);

var _resize = __webpack_require__(3);

var _profileEdit = __webpack_require__(2);

$(function () {
    // bootstrapping the applications
    (0, _mobileEvents.registerMobileEvents)();
    (0, _navigation.setupNavigation)();
    (0, _resize.setupResize)();
    (0, _profileEdit.setupProfileEdit)();
});

/***/ })
/******/ ]);